---
permalink: /about/
title: "About"
---

## About this site

Welcome to the personal website for **Numou the Impfox!** Currently a work in progress. This is where I would like to start blogging on a semi-regular basis, as well as to have my own place where I can post my own written stories. Until now I rely only on FurAffinity to host my stories and I would like to change that... having my own site where I can post whatever I want is ideal.

Once this site has some real content on it, let me know what you think!
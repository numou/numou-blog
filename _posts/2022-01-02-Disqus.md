---
title: "Comments provider changed to Disqus"
date: 2022-01-02
categories:
  - Announcements
tags:
  - update
  - comments
values:
  comments: true
---

While I didn't really want to do this, I could no longer justify paying **Hyvor Talk** $7.50 CAD a month just for a comments section that people weren't really using; maybe some day if my site gets a lot more traffic.

But for now, I've switched the comments provider to the free **Disqus**. I'm not crazy about Disqus, and you can probably Google the reasons why it's not really the best comment provider, but it's free, and while this site is in it's super low traffic stage, I need to pinch every penny I can.

Thank you for understanding. 💙

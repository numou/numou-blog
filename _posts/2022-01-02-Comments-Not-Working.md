---
title: "I can't get comments to work right now"
date: 2022-01-02T18:55:00Z
categories:
  - Announcements
tags:
  - update
  - comments
values:
  comments: true
---

So I thought changing the comments provider to Disqus would be a quick and easy thing.

I have it working in the Jekyll theme so far as it will load the Disqus section in, but Disqus will not load in a comments section, no matter what. The shortname is correct and everything. I am very frustrated right now. I don't understand a lot of this stuff so I may eventually give up and switch to something easy like Wordpress pretty soon. Managing my site with git and Jekyll is a pain in the ass.

Sorry about that.
